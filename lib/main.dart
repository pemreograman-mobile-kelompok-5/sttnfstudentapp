import 'package:flutter/material.dart';
import 'package:sttnf_student_app/pages/access_elen_page.dart';
import 'package:sttnf_student_app/pages/complaint_page.dart';
import 'package:sttnf_student_app/pages/create_complaint_page.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/complain-page',
      routes: {
        '/complain-page': (context) => ComplaintPage(),
        '/create-complain-page': (context) => CreateComplaintPage(),
        '/access-elen-page': (contex) => AccessELENPage(),
      },
    ),
  );
}
