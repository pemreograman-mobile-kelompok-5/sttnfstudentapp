import 'package:flutter/material.dart';
import 'package:sttnf_student_app/pages/widget/input_chat_widget.dart';
import 'package:sttnf_student_app/pages/widget/receiver_chat_widget.dart';
import 'package:sttnf_student_app/pages/widget/sender_chat_widget.dart';

class AccessELENPage extends StatefulWidget {
  AccessELENPage({Key key}) : super(key: key);

  @override
  _AccessELENPageState createState() => _AccessELENPageState();
}

class _AccessELENPageState extends State<AccessELENPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Akses ELEN"),
        backgroundColor: Color(0xFF005BAA),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0),
                ),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0),
                ),
                child: ListView.builder(
                  reverse: true,
                  padding: EdgeInsets.only(top: 15.0),
                  itemCount: 1,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        SenderChatWidget(),
                        ReceiverChatWidget(),
                        SizedBox( height: 30),
                      ],
                    );
                  },
                ),
              ),
            ),
          ),
          InputChatWidget(),
        ],
      ),
    );
  }
}
