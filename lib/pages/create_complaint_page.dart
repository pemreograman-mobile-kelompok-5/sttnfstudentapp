import 'package:flutter/material.dart';

class CreateComplaintPage extends StatefulWidget {
  CreateComplaintPage({Key key}) : super(key: key);

  @override
  _CreateComplaintPageState createState() => _CreateComplaintPageState();
}

class _CreateComplaintPageState extends State<CreateComplaintPage> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Buat Pengaduan"),
        backgroundColor: Color(0xFF005BAA),
      ),
      body: Container(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(padding: const EdgeInsets.symmetric(
                vertical: 20.0,
                horizontal: 20,
              ),
              child: TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Judul",
                ),
                validator: (value){
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 30.0,
                horizontal: 20,
            ),
            child: TextFormField(
              maxLines: null,
              keyboardType: TextInputType.multiline,
              minLines: 10,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                  labelText: "Deskripsi",
                  floatingLabelBehavior: FloatingLabelBehavior.always,
              ),
              validator: (value) {
                if (value.isEmpty){
                  return 'Please enter some text';
                }
                return null;
                },
              ),
            ),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 50.0,
                  horizontal: 20,
              ),
              child: RaisedButton(
                padding: const EdgeInsets.symmetric(
                  vertical: 20,
                  horizontal: 10,
                ),
                color: Colors.orange,
                onPressed: (){
                  // validate will return true if the form is valid, or false if
                  // the form is invalid.
                  if (_formKey.currentState.validate()){
                    // Process data.
                  }
                },
                child: Text(
                  'Kirim Pengaduan',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
              )
             )
            ],
          ),
        ),
      ),
    );
  }
}
