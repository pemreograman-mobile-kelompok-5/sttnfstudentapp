import 'package:flutter/material.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_1.dart';

class ReceiverChatWidget extends StatelessWidget {
  const ReceiverChatWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
	    child: getReceiver(
        ChatBubbleClipper1(type: BubbleType.receiverBubble),
        context,
      ),
    );

  }

  getReceiver(CustomClipper clipper, BuildContext context) => ChatBubble(
      clipper: clipper,
      backGroundColor: Color(0xffE7E7ED),
      margin: EdgeInsets.only(top: 20),
      child: Container(
        constraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width * 0.7,
        ),
        child: Text(
         "untuk user akun anda sudah kami buka kembali, Silahkan dicoba akses" ,
         style: TextStyle(
           color: Colors.black,
           fontSize: 15,
         ),
        ),
      ),
    );
}