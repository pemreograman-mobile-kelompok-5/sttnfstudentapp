import 'package:flutter/material.dart';

class InputChatWidget extends StatelessWidget {
  const InputChatWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: <Widget>[
        // Edit text
        Flexible(
          child: Container(
            padding: EdgeInsets.all(10),
            child: TextField(
              style: TextStyle(
                color: Colors.blue, 
                fontSize: 15.0,
              ),
              decoration: InputDecoration.collapsed(
                hintText: 'Type your message...',
              ), // InputDecoration.collapsed
            ), // TextField
          ), // Container
        ), // Flexible

        // Button send message
        Material(
          child: Container(
            child: IconButton(
              icon: Icon(Icons.send),
              onPressed: () {},
              color: Colors.blue,
            ), // IconButton
          ), // Container
          color: Colors.white,
        ), // Material
    ], // <Widget>[]
      ), //Row
      width: double.infinity,
      height: 50.0,
      decoration: BoxDecoration(color: Colors.white),
    ); //Container
  }
}