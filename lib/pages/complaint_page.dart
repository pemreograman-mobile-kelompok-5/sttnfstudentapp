import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:sttnf_student_app/pages/access_elen_page.dart';
import 'package:sttnf_student_app/pages/create_complaint_page.dart';

class ComplaintPage extends StatefulWidget {
  ComplaintPage({Key key}) : super(key: key);

  @override
  _ComplaintPageState createState() => _ComplaintPageState();
}

class _ComplaintPageState extends State<ComplaintPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pengaduan"),
        backgroundColor: Color(0xFF005BAA),
      ), // AppBar
      body: Container(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Card(
              child: ListTile(
                title: Text(
                  "Akses ELEN",
                ), // Text
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => AccessELENPage()),
                  );
                },
                trailing: Stack(
                  children: [
                    Icon(
                      Icons.forum,
                      color: Colors.black,
                    ), // Icon
                    Badge(
                      //child: Icon(Icons.settings),
                      shape: BadgeShape.circle,
                      child: Icon(Icons.forum),
                      badgeContent: Text("3"),
                    ), // Badge
                  ],
                ),
              ),
            ),
            Card(
              child: ListTile(
                title: Text(
                  "Lupa Password",
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: "Buat Pengaduan",
        backgroundColor: Colors.orange[600],
        child: Icon(
          Icons.support_agent,
          size: 35,
        ), // Icon
        onPressed: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CreateComplaintPage()),
          );
        },
      ),
    );
  }
}
