# kelompok5-STTNFStudentApp

Ini adalah pengerjaan Fitur STTNFStudentApp kelompok 5 yang terdiri dari:
1. Justis Aulia Pratomo (Ketua)<br>
--Username : @just_is
2. Abdillah Sagif<br>
--Username: @elbahmed
3. Abidun Alim<br>
--Username: @abidun
4. Achmad Fikri Abdillah<br>
--Username: @fikri_lil
5. Alif Zidan Raihan<br>
--Username: @alifzidanr
6. M. Fakhri Kuncoro Mukti<br>
--Username: @fakhrikuncoro
7. Tegar Budi Santoso<br>
--Username: @tegar04

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.